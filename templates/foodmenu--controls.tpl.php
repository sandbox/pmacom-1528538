<div class='foodmenu-admin-examples'></div>

<div name='foodmenu-control-title-dotted' class='foodmenu-control-btn foodmenu-control-title-dotted <?php print $defaults['menu_item_title_dotted'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Underline
</div>

<div name='foodmenu-control-price-options' class='foodmenu-control-btn foodmenu-control-price-options <?php print $defaults['menu_item_price_options'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Price Options
</div>

<div name='foodmenu-control-price-2' class='foodmenu-control-btn foodmenu-control-price-2 <?php print $defaults['menu_item_price_2'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Price 2
</div>

<div name='foodmenu-control-price-3' class='foodmenu-control-btn foodmenu-control-price-3 <?php print $defaults['menu_item_price_3'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Price 3
</div>

<div name='foodmenu-control-desc' class='foodmenu-control-btn foodmenu-control-desc <?php print $defaults['menu_item_desc'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Description
</div>

<div name='foodmenu-control-desc-prefix' class='foodmenu-control-btn foodmenu-control-desc-prefix <?php print $defaults['menu_item_desc_prefix'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Description Prefix
</div>

<div name='foodmenu-control-desc-suffix' class='foodmenu-control-btn foodmenu-control-desc-suffix <?php print $defaults['menu_item_desc_suffix'] ? 'active' : 'inactive'; ?>'>
  <div class='foodmenu-admin-btn-toggle'></div>
  Description Suffix
</div>

