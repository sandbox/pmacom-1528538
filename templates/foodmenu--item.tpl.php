<?php if($item['menu_type'] == '0'): ?>

  <div class="foodmenu-item">

    <?php if($item['menu_item_price_options']): ?>
      <div class='foodmenu-item-price-options'>
        <?php print $item['menu_item_price_options']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_price_1']): ?>
      <div class='foodmenu-item-price-1'>
        <?php print $item['menu_item_price_1']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_price_2']): ?>
      <div class='foodmenu-item-price-2'>
        <?php print $item['menu_item_price_2']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_price_3']): ?>
      <div class='foodmenu-item-price-3'>
        <?php print $item['menu_item_price_3']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_title']): ?>
      <div class='foodmenu-item-title <?php if($item['menu_item_title_dotted']){ print "dotted"; }?>'>
        <?php print $item['menu_item_title']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_desc_prefix']): ?>
      <div class='foodmenu-item-desc-prefix'>
        <?php print $item['menu_item_desc_prefix']; ?>
      </div>
    <?php endif; ?>
  
    <?php if($item['menu_item_desc']): ?>
      <div class='foodmenu-item-desc'>
        <?php print $item['menu_item_desc']; ?>
      </div>
    <?php endif; ?>

    <?php if($item['menu_item_desc_suffix']): ?>
      <div class='foodmenu-item-desc-suffix'>
        <?php print $item['menu_item_desc_suffix']; ?>
      </div>
    <?php endif; ?>

  </div>

<?php endif; ?>
