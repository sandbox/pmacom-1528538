<?php

/*
  $group_title
    Title of the group
  $group_icon
    Icon for the group
  $group_description
    Description Field
  $group_items
    Array of items
  
*/

?>

<div class='foodmenu-group'>

  <div class='foodmenu-group-title'>
    <div class='foodmenu-group-icon foodmenu-group-icon-food'></div>
    <h3>Group Title</h3>
  </div>

</div>
