(function ($) {

/*
  This js is sloppy. Very sloppy.
  TODO: Cleanup / Comments
*/

Drupal.behaviors.foodmenu = {
  attach: function (context) {

		$('.edit-field-foodmenu-controls-btn').once('foodmenu-controls', function() {

			// When another item is added, sometimes the dropdown and formitems become out of sync
			$.each( $(this).next().children('.foodmenu-control-btn'), function() {
				var foodmenu_form_item_name = $(this).attr('name').replace('control', 'item');

				if( foodmenu_form_item_name == 'foodmenu-item-title-dotted'){
					console.log( $(this).parent().parent().parent().find('.foodmenu-item-title-dotted' ) );
				}else{
					var foodmenu_form_item = $(this).parent().parent().next().next().find('.'+foodmenu_form_item_name);
					if( foodmenu_form_item.val() != '' ){
						if($(this).not('active')){ $(this).addClass('active'); }
					}
				}
				
			});

			// When the button is pressed
		  $(this).bind('click', function() {   			
		    if( $(this).next().not(':animated') ) {
		    	$(this).next().slideToggle('fast');
		    }
		  });

		});

//*****************************************************/
//
//	Individual buttons on the popup windows
//
//*******/

$('.edit-field-foodmenu-wrapper .form-item, .edit-field-foodmenu-wrapper .form-textarea-wrapper').once('foodmenu-load', function() {
	if($(this).children().val() != ''){
		$(this).children().show();		
	}
});

$('.foodmenu-control-btn').once('foodmenu-button', function() {

		  $(this).bind('click', function() {
			
				$(this).toggleClass('active');

				_foodmenu_form_item = _foodmenu_fetch_form_element(this).find('.'+$(this).attr('name').replace('control', 'item'));

				switch($(this).attr('name')){
					case 'foodmenu-control-title-dotted':
						var foodmenu_checkbox = _foodmenu_fetch_form_element(this).prev().find('input');
						foodmenu_checkbox.attr("checked", !foodmenu_checkbox.attr("checked") );
						$(this).parent().parent().next().next().find('.foodmenu-item-title').toggleClass('dotted');
						break;
					default: //'foodmenu-control-price-optons':
						_foodmenu_form_item.toggle();
				}
				
		  });
		});

/**************************************************************************/


$('.foodmenu-control-title-dotted').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-dotted');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-desc-suffix').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-desc-suffix');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-desc-prefix').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-desc-prefix');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-desc').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-desc');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-price-options').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-price-options');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-price-2').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-price-2');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

$('.foodmenu-control-price-3').hover(function(){
  // OnRollOver
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples foodmenu-admin-examples-price-2');
  },function(){
  // onRollOut
    $('.foodmenu-admin-examples').attr('class', '').addClass('foodmenu-admin-examples');
  });

	}
}
})(jQuery);


function _foodmenu_fetch_form_element(control_btn_element){
	return jQuery(control_btn_element).parent().parent().next().next();
}

function _foodmenu_fetch_form_control(item_form_element){
	return jQuery(item_form_element).parent().parent().next().next();
}
